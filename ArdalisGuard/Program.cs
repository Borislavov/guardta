﻿using Ardalis.GuardClauses;
using System;

namespace ArdalisGuard
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car = new Car("pesho", 400);
            Console.WriteLine($"{car.Engine} : {car.MaxSpeed}");
            car.MaxSpeed = 450;
            Console.WriteLine($"{car.Engine} : {car.MaxSpeed}");
        }
    }
    public class Car
    {
        private string engine;
        private int maxSpeed;

        public Car(string engine, int maxSpeed)
        {
            Engine = engine;
            MaxSpeed = maxSpeed;
        }

        public string Engine
        {
            get { return this.engine; }
            set
            {
                Guard.Against.NullOrEmpty(value, nameof(Engine));
                engine = value;
            }
        }
        public int MaxSpeed
        {
            get { return this.maxSpeed; }
            set
            {
                Guard.Against.OutOfRange(value, nameof(MaxSpeed), 0, 400);
                maxSpeed = value;
            }
        }
    }
}
