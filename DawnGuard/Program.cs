﻿using Dawn;
using System;

namespace DawnGuard
{
    class Program
    {
        static void Main(string[] args)
        {
            Person person = new Person("pesho", 5);
            Console.WriteLine($"{person.Name} : {person.Age}");
            person.Age = -5;
            Console.WriteLine($"{person.Name} : {person.Age}");
        }
    }
    public class Person
    {
        private string name;
        private int age;

        public Person(string name, int age)
        {
            Name = Guard.Argument(name, nameof(name)).NotNull().NotEmpty();
            Age = Guard.Argument(age, nameof(age)).NotNegative();
        }

        public string Name
        {
            get { return this.name; }
            set { name = Guard.Argument(value, nameof(name)).NotNull().NotEmpty(); }
        }
        public int Age
        {
            get { return this.age; }
            set { age = Guard.Argument(value, nameof(age)).Positive(); }
        }
    }
}

